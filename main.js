const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
const DRINK_BASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
const VOUCHER_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
class Main {
    constructor() {
        $(document).ready(() => {
            this.page = new RenderPage()
            this.page.renderPage()
            this.order = new CreateOrder()
            this.order.createNewOrder()
        })
    }

}
new Main()
class RenderPage {
    constructor() {
        this.api = new CallApi()
    }
    _getDrinkList() {
        this.api.onGetDrinkListClick((drinkList) => {
            this._renderDrinks(drinkList)
        })
    }
    _renderDrinks(drinkList) {
        console.log(drinkList)
        const $select = $('#drink-type');
        drinkList.forEach((drink) => {
            $select.append($('<option>', {
                value: drink.maNuocUong,
                text: drink.tenNuocUong
            }));
        });
    }
    renderPage() {
        this._getDrinkList()
    }

}

class CreateOrder {
    constructor() {
        this.api = new CallApi()
        this.modal = new Modal()
    }
    _getSize(order, card, button) {
        const buttons = $('.choose-size-btn');
        if (button.text() === 'Chọn') {
            buttons.removeClass('active').text('Chọn');
            button.addClass('active').text('Bỏ chọn');
            const duongKinh = card.find('.duong-kinh').text();
            const suon = card.find('.suon').text();
            const salad = card.find('.salad').text();
            const tien = card.find('.thanhtien').text();
            const tienNumber = parseInt(tien.replace(".", "").replace("VND", ""));
            const nuoc = card.find('.nuoc').text();
            const size = button.val();
            order.soLuongNuoc = nuoc
            order.size = size;
            order.duongKinh = duongKinh;
            order.suon = suon;
            order.salad = salad;
            order.thanhtien = tienNumber
        } else {
            button.removeClass('active').text('Chọn');
        }
    }
    _getPizzaType(order, button) {
        const buttons = $('.choose-pizza-btn');
        if (button.text() === 'Chọn') {
            buttons.removeClass('active').text('Chọn');
            button.addClass('active').text('Bỏ chọn');
            const selectedValue = button.val();
            order.loaiPizza = selectedValue;
        } else {
            button.removeClass('active').text('Chọn');
        }
    }
    _getDrinkType(order, select) {
        const nuoc = select.val()
        order.idLoaiNuocUong = nuoc
    }
    _checkValidatePromCode(promoCode) {
        const promoCodeInput = $('#input-promoCode')
        this._clearInValid(promoCodeInput)
        this.api.onCheckVoucherIdClick(promoCode, (data) => {
            if (data === "invalid") {
                console.log(data)
                promoCodeInput.addClass('is-invalid');
                promoCodeInput.removeClass('border-success');
                promoCodeInput.after(`<div class="invalid-feedback error-message"> Mã không đúng, vui lòng nhâp mã khác!</div>`)

            } else {
                console.log(data)
                promoCodeInput.removeClass('is-invalid');
                promoCodeInput.addClass('is-valid border-success')
                promoCodeInput.next('.invalid-feedback').html('');
            }
        });
    }

    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _submitOrder(order) {
        this._clearInValid()
        let isValid = true;
        const name = $('#input-name').val();
        const email = $('#input-email').val();
        const phoneNumber = $('#input-number').val();
        const address = $('#input-address').val();
        const promoCode = $('#input-promoCode').val();
        const message = $('#input-message').val();
        const fields = ["input-name", "input-email", "input-number", "input-address"]
        fields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
                isValid = false
                $(`#${field}`).addClass("is-invalid");
                $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ cho ${field.replace("-", " ")}.</div>`);
            }
        })
        if (!order.loaiPizza || !order.size) {
            isValid = false
            this.api.onShowToast("Invalid", "Please choose size and pizza")
        }

        if (isValid) {
            const createOrder = {
                kichCo: order.size,
                duongKinh: order.duongKinh,
                suon: +order.suon,
                salad: order.salad,
                loaiPizza: order.loaiPizza,
                idVourcher: promoCode,
                idLoaiNuocUong: order.idLoaiNuocUong,
                soLuongNuoc: +order.soLuongNuoc,
                hoTen: name,
                thanhTien: order.thanhtien,
                email: email,
                soDienThoai: phoneNumber,
                diaChi: address,
                loiNhan: message
            };

            this.modal.openModal(createOrder)
            // this.api.onCreateOrderClick(createOrder)
        }
    }
    _choosePizza() {
        let order = {};
        $('.choose-size-btn').click((event) => {
            const card = $(event.target).closest('.card');
            const button = $(event.target);
            this._getSize(order, card, button);
        });

        $('.choose-pizza-btn').click((event) => {
            const button = $(event.target);
            this._getPizzaType(order, button);
        });

        $("#drink-type").on("change", (event) => {
            const select = $(event.target);
            this._getDrinkType(order, select);
        });
        $('#input-promoCode').on("blur", (event) => {
            const promCode = event.target.value
            if (!promCode) {
                this._clearInValid()
                return
            }
            this._checkValidatePromCode(promCode)
        });

        $('#order-form').submit((event) => {
            event.preventDefault();
            this._submitOrder(order);
        });
    }
    createNewOrder() {
    
        this._choosePizza()
    }
}

class Modal {
  
    constructor() {
        this.api = new CallApi()
    }
    _renderOrderInfo(orderInfo) {
        let promoCode = 0
        this.api.onCheckVoucherIdClick(orderInfo.idVourcher, (data) => {
            if(data.phanTramGiamGia) {
                promoCode = +data.phanTramGiamGia
            } 
            $('#name-input').val(orderInfo.hoTen);
            $('#phone-input').val(orderInfo.soDienThoai);
            $('#address-input').val(orderInfo.diaChi);
            $('#message-input').val(orderInfo.loiNhan);
            $('#promoInput').val(orderInfo.idVourcher);
            const formattedPrice = orderInfo.thanhTien.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
            const formattedDiscountedPrice = (orderInfo.thanhTien * (1 - promoCode/100)).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
            const orderDetails = 
            `\t\tXác nhận: ${orderInfo.hoTen}, ${orderInfo.soDienThoai}, ${orderInfo.diaChi}
            \tMenu: ${orderInfo.kichCo}, Sườn nướng ${orderInfo.suon} nước ${orderInfo.soLuongNuoc}
            \tLoại pizza: ${orderInfo.loaiPizza}, Giá: ${formattedPrice}, Mã giảm giá: ${orderInfo.idVourcher}
            \tPhải thanh toán: ${formattedDiscountedPrice} Giảm: ${promoCode}%`;
            $('#details-input').val(orderDetails);
            
        })

    }
    _closeModal () {
        $(".cancel").click(()=> {
            $('#orderModal').modal('hide'); 
        })

        $(".btn-close").click(()=> {
            $('#orderModal').modal('hide'); 
        })
    }

    _sendOrder(orderInfo) {
        $(".confirm").click(()=>{
           this.api.onCreateOrderClick(orderInfo)
        })
    }

    openModal(orderInfo) {
        this._closeModal()
        this._sendOrder(orderInfo)
        this._renderOrderInfo(orderInfo)
        $('#orderModal').modal('show');

    }

}

class CallApi {

    constructor() {

    }
    onShowToast(title, message) {
        $('#myToast .mr-auto').text(title)
        $('#myToast .toast-body').text(message);
        $('#myToast').toast('show');
    }
    onCreateOrderClick(order) {
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            data: JSON.stringify(order),
            contentType: "application/json;charset=UTF-8",
            success: (order) => {

                console.log(order)
                this.onShowToast("Đặt thành công", "Chúc mừng bạn đã đặt thành công")
                $('#orderModal').modal('hide');
                $('#thank-order').modal('show'); 
                 $("#order-id").val(order.orderCode)
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }


    onGetDrinkListClick(callback) {
        $.ajax({
            url: DRINK_BASE_URL,
            type: "GET",
            success: function (response) {
                callback(response)
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }
    onCheckVoucherIdClick(promoCode, callback) {
        // một số mã đúng để test: 95531, 81432,...lưu ý test cả mã sai
        $.ajax({
            url: VOUCHER_URL + promoCode,
            method: "GET",
            success: function (data) {
                callback(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // không nhận lại được data do vấn đề gì đó: khả năng mã voucher ko đúng
                console.log("invalid " + jqXHR.responseText);
                callback("invalid")
            }
        });
    }

}